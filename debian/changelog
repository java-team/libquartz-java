libquartz-java (1:1.8.6-8) unstable; urgency=medium

  * Add patch for XXE vulnerability CVE-2019-13990 (Closes: #933169)

 -- tony mancill <tmancill@debian.org>  Mon, 30 Jan 2023 22:23:39 -0800

libquartz-java (1:1.8.6-7) unstable; urgency=medium

  * Add build-dep on liblog4j1.2-java (Closes: #1028678)
  * Use debhelper-compat (=13)
  * Set Rules-Requires-Root: no in debian/control
  * Bump Standards-Version to 4.6.2
  * Replace libservlet3.1-java with libservlet-api-java
  * Freshen years in debian/copyright

 -- tony mancill <tmancill@debian.org>  Sun, 29 Jan 2023 20:07:14 -0800

libquartz-java (1:1.8.6-6) unstable; urgency=medium

  * Team upload.
  * Fixed the build failure with Java 11 (Closes: #911182)
  * Standards-Version updated to 4.2.1
  * Switch to debhelper level 11
  * Use salsa.debian.org Vcs-* URLs

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 16 Oct 2018 23:57:28 +0200

libquartz-java (1:1.8.6-5) unstable; urgency=medium

  * Team upload.
  * Removed the -java-doc package to fix the build failure with Java 9
    (Closes: #874651)
  * Replaced the build dependency on glassfish-j2ee with the Geronimo spec jars
  * Standards-Version updated to 4.1.2

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 04 Dec 2017 11:59:10 +0100

libquartz-java (1:1.8.6-4) unstable; urgency=medium

  * Add myself to uploaders
  * Add patch to disable phone-home update check (Closes: #864769)
  * Bump Standards-Version to 4.0.0

 -- tony mancill <tmancill@debian.org>  Sun, 25 Jun 2017 10:50:34 -0700

libquartz-java (1:1.8.6-3) unstable; urgency=medium

  * Team upload.
  * Removed the unused dependency on libcommons-modeler-java
  * Switch to debhelper level 10

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 16 Nov 2016 16:58:52 +0100

libquartz-java (1:1.8.6-2) unstable; urgency=medium

  * Team upload.
  * Build with the DH sequencer instead of CDBS
  * Build depend on libasm-java (>= 5.0) instead of libasm4-java
  * Removed the unused build dependency on glassfish-activation
  * Standards-Version updated to 3.9.8 (no changes)
  * Use secure Vcs-* fields
  * Changed debian/watch to track only the 1.x releases

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 26 Sep 2016 10:56:17 +0200

libquartz-java (1:1.8.6-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - Ignore the new modules (commonj, oracle, weblogic, all)
    - New build dependency on libhamcrest-java
  * Suggest the optional dependencies instead of recommending them
  * Standards-Version updated to 3.9.6 (no changes)
  * Use XZ compression for the upstream tarball
  * Moved the package to Git

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 21 Oct 2015 13:13:51 +0200

libquartz-java (1:1.7.3-5) unstable; urgency=medium

  * Team upload
  * Build depend on libasm4-java instead of libasm3-java
  * Build depend on libmail-java instead of glassfish-mail

 -- Emmanuel Bourg <ebourg@apache.org>  Sat, 13 Sep 2014 10:26:21 +0200

libquartz-java (1:1.7.3-4) unstable; urgency=low

  * Team upload

  [ Mathieu Malaterre ]
  * Remove self from Uploaders

  [ Emmanuel Bourg ]
  * debian/control:
    - Build depend on libservlet3.1-java instead of libservlet2.5-java
    - Use canonical URLs for the Vcs-* fields
    - Updated Standards-Version to 3.9.5 (no changes)
  * Switch to debhelper level 9

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 08 Sep 2014 00:32:08 +0200

libquartz-java (1:1.7.3-3) unstable; urgency=low

  * Revert to release 1.7.3 since 2.x is not compatible. Closes: #669080
   - This upload with an epoch value overwrite a 2.1.3 upload which broke
     binary compat. See also #669081 for more info on epoch reason.

 -- Mathieu Malaterre <malat@debian.org>  Sat, 21 Apr 2012 10:36:06 +0200

libquartz-java (2.1.3-1) unstable; urgency=low

  * New upstream. Closes: #667598
  * Add myself as uploader,
  * Remove Michael Koch from uploaders. Closes: #654091
  * Bump Std-Vers to 3.9.3, no changes needed

 -- Mathieu Malaterre <malat@debian.org>  Mon, 09 Apr 2012 17:51:52 +0200

libquartz-java (1.7.3-2) unstable; urgency=low

  * Add missing Build-Depends: libcommons-validator-java. (Closes: #643503)
  * Fix lintian warnings for debian/copyright.

 -- Torsten Werner <twerner@debian.org>  Tue, 27 Sep 2011 20:03:48 +0200

libquartz-java (1.7.3-1) unstable; urgency=low

  * New upstream release.
    - Do not build modules quartz-jboss and examples.
  * Update d/watch and d/orig-tar.sh to create the orig tarball from the SVN
    repo.
  * Update Homepage field.
  * Use Maven to build package because upstream has switched to Maven.
  * Switch to source format 3.0.
  * Remove Java runtime from Depends.
  * Add a *-doc package.

 -- Torsten Werner <twerner@debian.org>  Wed, 17 Aug 2011 13:57:53 +0200

libquartz-java (1.6.6-1) unstable; urgency=low

  * New upstream release.
  * Removed ${shlibs:Depends} from Depends.

 -- Michael Koch <konqueror@gmx.de>  Wed, 04 Nov 2009 17:01:27 +0100

libquartz-java (1.6.5-1) unstable; urgency=low

  * New upstream release.
  * Build-Depends on default-jdk.
  * Build-Depends on debhelper (>= 7).
  * Moved package to section 'java'.
  * Added myself to Uploaders.
  * Updated Standards-Version to 3.8.3.

 -- Michael Koch <konqueror@gmx.de>  Fri, 02 Oct 2009 22:47:16 +0200

libquartz-java (1.6.4-1) unstable; urgency=low

  * New upstream release

 -- Torsten Werner <twerner@debian.org>  Fri, 05 Dec 2008 00:16:49 +0100

libquartz-java (1.6.2+dak1-2) unstable; urgency=low

  * Fix symlink version, thanks to Damien Raude-Morvan (Closes: #507323)

 -- Varun Hiremath <varun@debian.org>  Mon, 01 Dec 2008 02:30:16 -0500

libquartz-java (1.6.2+dak1-1) unstable; urgency=low

  * Move package to main.

 -- Torsten Werner <twerner@debian.org>  Tue, 18 Nov 2008 21:49:17 +0100

libquartz-java (1.6.2-1) unstable; urgency=low

  * New upstream release
  * Bump up Standards-Version: 3.8.0 (no changes).
  * Do not quote the full text of the Apache license any more.

 -- Torsten Werner <twerner@debian.org>  Sat, 08 Nov 2008 23:23:30 +0100

libquartz-java (1.6.1~RC1-1) unstable; urgency=low

  * New upstream release
  * Bump Standards-Version to 3.7.3
  * debian/rules: fix jar symlink version

 -- Varun Hiremath <varun@debian.org>  Fri, 02 May 2008 12:46:57 +0530

libquartz-java (1.6.0-2) unstable; urgency=low

  * Move package to contrib because of Build-Depends: glassfish-javaee.
  * Use upstream provided build.xml.

 -- Torsten Werner <twerner@debian.org>  Fri, 30 Nov 2007 14:10:48 +0100

libquartz-java (1.6.0-1) unstable; urgency=low

  * Initial release (Closes: #450612)

 -- Varun Hiremath <varunhiremath@gmail.com>  Tue, 24 Apr 2007 17:19:10 +0530
